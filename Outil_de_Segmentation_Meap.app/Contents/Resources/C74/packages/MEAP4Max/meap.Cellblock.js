

var	nowsize;
var lastsize;

var pollTsk = new Task(getsize, this);


function on()
{
	lastsize = -1;
	//post("A");

	pollTsk.interval = 150;
	
	pollTsk.repeat();
}

function off()
{
	pollTsk.cancel();
}
	
		
function getsize()
{	

	nowsize = this.patcher.wind.size;
	
	if (nowsize[0] != lastsize[0] || nowsize[1] != lastsize[1]) 
	{
		
		this.patcher.message("script", "sendbox","cellblock", "size", nowsize[0]-120, nowsize[1]-170);
		this.patcher.message("script", "sendbox","cellblockHeaders", "size", nowsize[0]-137, 15);
	
		lastsize = nowsize;
		
	}
	
}			
				
					
//function bang() {
//outlet(0,this.patcher.wind.size);
//}