//11olsen, subfolders to outlet 1, files to outlet 0, done to outlet 2

autowatch=1;
outlets=3;

function folder(arg){
    
    re = /(?:\.([^.]+))?$/;
    f = new Folder(arg);
    f.next();
    
    while (!f.end) 
	{
    
    	if (f.filetype=="fold") {outlet(1, arg+f.filename+"/");} 
    
    	else {outlet(0, arg+f.filename);}                         
    
    
    	//post(f.filename, f.filetype);
    	//post();

    	f.next();
    }

    f.close();
    outlet(2, "done");
}

function test(arg){  
re = /(?:\.([^.]+))?$/;
ext = re.exec(arg)[1];
post(arg.toUpperCase());
post(ext);
}